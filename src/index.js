var FRAME_RATE  = 1 / 120
  , DRAW_RADIUS = 5
  , projectiles = []
  , ctx
  , init
  , fireNewProjectile
  , calculateFramePositions
  , cleanOutOfBoundsProjectiles
  , redrawProjectiles
  , drawFrame;

init = function () {
  var canvas       = window.document.getElementById('canvas')
    , drawInterval = FRAME_RATE * 1000;

  ctx              = canvas.getContext('2d');
  ctx.fillStyle    = '#545454';
  canvas.onmouseup = fireNewProjectile;

  setInterval(drawFrame, drawInterval);
};


fireNewProjectile = function (e) {
  if (1 !== e.which) { return; }

  var xPos = e.pageX - ctx.canvas.offsetLeft
    , yPos = e.pageY - ctx.canvas.offsetTop
    , xVel = Math.random() * 40 - 20
    , yVel = Math.random() * 40 - 20
    , projectile;

  projectile = {
    position: { x: xPos, y: yPos },
    velocity: { x: xVel, y: yVel },
    massKg  : 250,
    radiusM : 0.25,
  };

  projectiles.push(projectile);
};


calculateFramePositions = function (projectiles) {
  var DRAG_COEFF = 0.47   // depends on the object geometry :: https://en.wikipedia.org/wiki/Drag_coefficient
    , RHO        = 1.204  // air @ 20°C :: https://en.wikipedia.org/wiki/Density#Air
    , GRAVITY    = 9.81   // earth gravity
    , BOUNCE_DAMPING_FACTOR = 0.65    // randomly selected damping factor
    , POSITION_SCALE        = 10;     // adjust the scale for the drawing area

  // =====================================================================================
  // Movement physics => drag equation: https://en.wikipedia.org/wiki/Drag_equation
  // Drag force: Fd = -1/2 * Cd * A * rho * v^2
  // =====================================================================================

  projectiles.forEach(function (projectile) {
    var A, Fx, Fy, ax, ay;

    // frontal projection area
    A = Math.PI * Math.pow(projectile.radiusM, 2);

    // Drag force components (Math.sign() => velocity direction)
    Fx = -0.5 * DRAG_COEFF * A * RHO * Math.pow(projectile.velocity.x, 2) * Math.sign(projectile.velocity.x);
    Fy = -0.5 * DRAG_COEFF * A * RHO * Math.pow(projectile.velocity.y, 2) * Math.sign(projectile.velocity.y);

    // acceleration from drag force (F=ma)
    ax = Fx / projectile.massKg;
    ay = (Fy / projectile.massKg) + GRAVITY;

    // delta-frame velocity :: v=a*t
    projectile.velocity.x += ax * FRAME_RATE;
    projectile.velocity.y += ay * FRAME_RATE;

    // delta-frame position (factor: compensate drawing scale) :: s=v*t
    projectile.position.x += projectile.velocity.x * FRAME_RATE * POSITION_SCALE;
    projectile.position.y += projectile.velocity.y * FRAME_RATE * POSITION_SCALE;

    // bounce-off of bottom => flip y- speed and position with speed dampening factor
    if (projectile.position.y >= (ctx.canvas.height - DRAW_RADIUS)) {
      projectile.velocity.y = -projectile.velocity.y * BOUNCE_DAMPING_FACTOR;
      projectile.position.y = 2 * (ctx.canvas.height - DRAW_RADIUS) - projectile.position.y;
    }
  });
};


cleanOutOfBoundsProjectiles = function (projectiles) {
  return projectiles.filter(function (projectile) {
    var isOutOfBounds =
        projectile.position.x > ctx.canvas.width ||
        projectile.position.y > ctx.canvas.height ||
        projectile.position.x < 0 ||
        projectile.position.y < 0;
    return !isOutOfBounds;
  });
};


redrawProjectiles = function (projectiles) {
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  projectiles.forEach(function (projectile) {
    ctx.save();
    ctx.translate(projectile.position.x, projectile.position.y);
    ctx.beginPath();
    ctx.arc(0, 0, DRAW_RADIUS, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.closePath();
    ctx.restore();
  });
  ctx.clearRect(0, 0, 1, 1);
};


drawFrame = function () {
  calculateFramePositions(projectiles);
  projectiles = cleanOutOfBoundsProjectiles(projectiles);
  redrawProjectiles(projectiles);
};


window.yfy = {
  init: init
};
