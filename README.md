# Yieldify Test

The solution is implemented using HTML canvas and JavaScript. Requirements
to run the solution is simply a modern browser.

Source file with the solution code can be found in `src/` directory.

## Instructions to build

Build contains of running jscs and jshint rules against the source JS file, and
minifying it with adding the IIFE wrapper, to avoid global namespace pollution.

Build tool used is gulp. To build the source, simply run:

    $ npm install   # to fetch build dependencies
    $ gulp

Built file can be found in `dist/` directory.

## Instructions to run

Open `index.html` in the browser and click on the canvas to fire new projectiles.

_Note: Built code in `dist/` directory has been committed to the repo for
simplicity. Otherwise, a build step would need to be executed before
trying to run the HTML file._
