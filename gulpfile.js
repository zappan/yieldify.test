var gulp   = require('gulp')
  , jscs   = require('gulp-jscs')
  , jshint = require('gulp-jshint')
  , uglify = require('gulp-uglify')
  , iife   = require('gulp-iife')
  , errorHandler;

errorHandler = function errorHandler(error) {
  console.log(error.toString());
  this.emit('end');
};

gulp.task('default', function () {
  return gulp.src('src/index.js')
    .pipe(jscs())
    .pipe(jscs.reporter())
    .pipe(jscs.reporter('fail')).on('error', errorHandler)
    .pipe(jshint())
    .pipe(jshint.reporter())
    .pipe(jshint.reporter('fail')).on('error', errorHandler)
    .pipe(iife({
      useStrict       : true,
      trimCode        : true,
      prependSemicolon: false,
      bindThis        : false,
      params          : ['window', 'document', 'undefined'],
      args            : ['window', 'document']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});
